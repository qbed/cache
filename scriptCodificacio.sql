use QbedHIS
select SincroEq.CacheId,Ordre,TipusCodi,CodiICD9Id,DescripcioICD9,
'^["WSS"]WSI901("'+SincroEq.CacheId+'","'+TipusCodi+'",'+CAST(Ordre AS NVARCHAR)+')="'+CodiICD9Id+'#'+DescripcioICD9+'"' AS CACHE
from CoCodificacioLinia 
inner join CoCodificacio on CoCodificacioLinia.CoCodificacioId=CoCodificacio.Id
INNER JOIN SincroEq ON SincroEq.SqlId=CoCodificacioLinia.CoCodificacioId
where CoCodificacio.UsuariCreacio<>'SINCRO'  AND SincroEq.TaulaSQL='AssAssistencia'
UNION
select SincroEq.CacheId,Ordre,TipusCodi,CodiICD9Id,DescripcioICD9,
'^["HSS"]HSS12("'+SincroEq.CacheId+'","'+TipusCodi+'",'+CAST(Ordre AS NVARCHAR)+')="'+CodiICD9Id+'#'+DescripcioICD9+'"' AS CACHE
 From VaValoracioCMBDSS 
inner join CoCodificacio on VaValoracioCMBDSS.AssistenciaId=CoCodificacio.Id
inner join CoCodificacioLinia on CoCodificacio.Id=CoCodificacioLinia.CoCodificacioId
INNER JOIN SincroEq ON SincroEq.SqlId=VaValoracioCMBDSS.Id
where CoCodificacio.UsuariCreacio<>'SINCRO' AND SincroEq.TaulaSQL='VaValoracioCMBDSS'
AND CAST(VaValoracioCMBDSS.DataCmbd AS DATE)>=CAST('2018-07-01' AS date)
ORDER BY SincroEq.CacheId,TipusCodi,Ordre